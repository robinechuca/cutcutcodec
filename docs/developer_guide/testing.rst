.. _testing:

Testing
=======

To make sure the installation has gone whell, or if you're contributing to the project, please run the test bench.
For running tests, some dependencies are requiered, you can install it passing the option ``[test]`` to ``pip``.
for more details, please refer to :ref:`full_installation`:

.. code:: shell

    clear && cutcutcodec test

If you want to skip certain parts, such as slow tests or code quality, refer to the testing api:

.. code:: shell

    cutcutcodec test --help

.. note::

    Go to the root of the git folder (do not enter the second cutcutcodec folder).
