.. _full_installation:

Full Installation
=================


Python and FFmpeg Support
-------------------------

CutCutCodec supports these versions.

.. csv-table:: Python versions
    :file: supported-python-versions.csv
    :header-rows: 1

.. csv-table:: FFmpeg versions
    :file: supported-ffmpeg-versions.csv
    :header-rows: 1

.. _Debian Ubuntu Mint System:
.. _RHEL CentOS Fedora System:
.. _Arch Manjaro System:
.. _OpenSUSE System:
.. _Linux Installation:
.. _FreeBSD Installation:
.. _macOS Installation:
.. _Windows Installation:


.. _virtual_environement:

Virtual Environement
--------------------

It is preferable to install CutCutCodec in a virtual environment. Please refer to the `pyenv main page <https://github.com/pyenv/pyenv>`_. It is possible to use ``python3-venv`` or ``conda`` as well.

Install pyenv
^^^^^^^^^^^^^

First install the `python dependencies <https://github.com/pyenv/pyenv/wiki#suggested-build-environment>`_ then install pyenv.

.. tab:: Linux

    .. tab:: Debian Ubuntu Mint System

        .. code-block:: shell

            sudo apt install libedit-dev libncurses5-dev
            sudo apt install build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev curl git libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev

    .. tab:: RHEL CentOS Fedora System

        .. code-block:: shell

            sudo yum install openssl11-devel --allowerasing
            yum install gcc make patch zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel tk-devel libffi-devel xz-devel

    .. tab:: Arch Manjaro System

        .. code-block:: shell

            yay -S ncurses5-compat-libs

    .. code-block:: shell

        curl https://pyenv.run | bash
        echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
        echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
        echo 'eval "$(pyenv init -)"' >> ~/.bashrc
        echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc
        source ~/.bashrc

.. tab:: macOS

    You could install dependencies by using `Homebrew <https://brew.sh/>`_.

    .. code-block:: shell

        brew install ncurses
        brew install openssl readline sqlite3 xz zlib tcl-tk
        brew install pyenv

If it fails, `this debug link <https://github.com/pyenv/pyenv/wiki/Common-build-problems>`_ may help you.

Configure pyenv
^^^^^^^^^^^^^^^

Create the virtual environement.

.. code-block:: shell

    pyenv update
    env PYTHON_CONFIGURE_OPTS="--enable-shared --enable-optimizations --with-lto" PYTHON_CFLAGS='-march=native -mtune=native' \
    pyenv install -v 3.13
    pyenv virtualenv 3.13 cutcutcodec
    pyenv activate cutcutcodec


.. _dependencies:

Dependencies
------------

gcc
^^^

As part of CutCutCodec is written in C, **gcc** must be installed. You can test the correct installation of gcc with the  ``gcc --version`` command.

.. tab:: Linux

    It is installed by default on many linux systems.

    .. tab:: Debian Ubuntu Mint System

        .. code-block:: shell

            sudo apt install build-essential
            sudo apt-get install manpages-dev

    .. tab:: RHEL CentOS Fedora System

        .. code-block:: shell

            sudo yum group install "Development Tools"
            sudo yum install man-pages

    .. tab:: Arch Manjaro System

        .. code-block:: shell

            sudo pacman -S base-devel
            sudo pacman -S gcc-multilib lib32-gcc-libs
            wget https://ftp.gnu.org/gnu/gcc/gcc-13.2.0/gcc-13.2.0.tar.gz
            tar -xzvf gcc-13.2.0.tar.gz
            cd gcc-13.2.0
            mkdir build
            cd build
            \../configure
            make -j4
            sudo make install

    .. tab:: OpenSUSE System

        .. code-block:: shell

            sudo zypper addrepo http://download.opensuse.org/distribution/leap/15.6/repo/oss/ oss
            zypper search gcc
            sudo zypper install gcc
            sudo zypper install gcc-c++

.. tab:: FreeBSD

    It is install by default on FreeBSD.

.. tab:: macOS

    You could install gcc by using `Homebrew <https://brew.sh/>`_.

    .. code-block:: shell

        brew install gcc

.. tab:: Windows

    .. warning:: I see that you insist on using Windows, this step is the most critical, good luck! Without gcc, some functions of cutcutcodec will be around **1000 times slowler**.

    It is not too late to listen the voice of reason! You can `install ubuntu <https://lecrabeinfo.net/installer-ubuntu-22-04-lts-le-guide-complet.html>`_ for example.

ffmpeg
^^^^^^

CutCutCodec has hard dependency on the FFmpeg package. You should install it first, please refer to the `FFmpeg download page <https://ffmpeg.org/download.html>`_.

.. tab:: Linux

    .. tab:: Debian Ubuntu Mint

        .. code:: shell

            sudo apt install ffmpeg
            sudo apt install python-dev-is-python3 pkg-config
            sudo apt install libavformat-dev libavcodec-dev libavdevice-dev libavutil-dev libswscale-dev libswresample-dev libavfilter-dev

    .. tab:: RHEL CentOS Fedora

        .. code:: shell

            sudo yum install ffmpeg

    .. tab:: Arch Manjaro

        .. code:: shell

            sudo pacman -S ffmpeg

    .. tab:: OpenSUSE

        .. code:: shell

            sudo zypper install ffmpeg

.. tab:: FreeBSD

    .. code:: shell

        sudo pkg install ffmpeg

.. tab:: macOS

    You could install the FFmpeg by using `Homebrew <https://brew.sh/>`_.

    .. code:: shell

        brew install ffmpeg pkg-config

.. tab:: Windows

    .. warning:: Windows is crap, so be prepared for a tedious and buggy installation! You should forget Microchiotte-Windaube and go straight to Linux before you pull out all your hair!

    It is important to configure your environement variable to hook ffmpeg to CutCutCodec. You can follow `this guide <https://www.geeksforgeeks.org/how-to-install-ffmpeg-on-windows/>`_ for example.

imagemagik
^^^^^^^^^^

To read certain images, you need to `install wand <https://docs.wand-py.org/en/latest/guide/install.html>`_.

.. tab:: Linux

    .. tab:: Debian Ubuntu Mint

        .. code:: shell

            sudo apt-get install libmagickwand-dev

    .. tab:: RHEL CentOS Fedora

        .. code:: shell

            sudo yum install ImageMagick-devel

.. tab:: FreeBSD

    .. code:: shell

        pkg_add -r py-wand

.. tab:: macOS

    You could install the FFmpeg by using `Homebrew <https://brew.sh/>`_.

    .. code:: shell

        brew install imagemagick
        export MAGICK_HOME=/opt/homebrew


pyav (optional)
^^^^^^^^^^^^^^^

Although it is installed automatically, it is better to install **av** manually to avoid redundancy with ffmpeg. Please refer to the `PyAv installation guide <https://pyav.org/docs/develop/overview/installation.html>`_.

.. tab:: Linux

    .. code:: shell

        pip -v install av --no-binary av

.. tab:: FreeBSD

    .. code:: shell

        pip -v install av --no-binary av

.. tab:: macOS

    On **MacOS** you may have issues with regards to Python expecting gcc but finding clang. Try to export the following before installation:

    .. code:: shell

        export ARCHFLAGS=-Wno-error=unused-command-line-argument-hard-error-in-future
        pip -v install av --no-binary av

.. tab:: Windows

    .. warning:: I see that you insist on using Windows, this step is the most critical, good luck!

    On **Windows** you must build from source in order to indicate the location of ffmpeg, pleas follow the `PyAv windows installation guide <https://pyav.org/docs/develop/overview/installation.html#windows>`_ and good luck!

cuda (optional)
^^^^^^^^^^^^^^^

If you have a GPU, please install CUDA or ROC then follow the `PyTorch installation guide <https://pytorch.org/>`_. Without CUDA or ROC, the software is not able to use the GPU (CPU only).

.. tab:: Linux

    Follow the official `NVIDIA CUDA Installation Guide for Linux <https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html>`_.

.. tab:: FreeBSD

    It is not well supproted, you can try to follow `this bsd link <https://gist.github.com/Mostly-BSD/4d3cacc0ee2f045ed8505005fd664c6e>`_.

.. tab:: macOS

    Follow the `NVIDIA CUDA Installation Guide for Mac OS X <https://docs.nvidia.com/cuda/archive/10.2/cuda-installation-guide-mac-os-x/index.html>`_.

.. tab:: Windows

    .. warning:: Windows is crap, so be prepared for a tedious and buggy installation! You should forget Microchiotte-Windaube and go straight to Linux before you pull out all your hair!

    It is important to configure your environement variable to hook cuda to PyTorch.
    Good luck with the `official guide <https://docs.nvidia.com/cuda/cuda-installation-guide-microsoft-windows/index.html>`_.


Installation
------------

Basic Installation
^^^^^^^^^^^^^^^^^^

.. note::

    The following instructions will install CutCutCodec with simple support for graphical interface.
    See :ref:`building_from_source` for a complete installation including the documentation and the tests.

To install the lastest development version, use ``pip``:

.. code:: shell

    pip install git+https://framagit.org/robinechuca/cutcutcodec.git

.. _building_from_source:

Building From Source (For devellopers)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To install the lastest development version from `Framagit <https://framagit.org/robinechuca/cutcutcodec>`_ source, clone cutcutcodec using ``git`` and install it using ``pip``:

.. code:: shell

    git clone https://framagit.org/robinechuca/cutcutcodec.git ~/cutcutcodec_git;
    cd ~/cutcutcodec_git
    pyenv activate cutcutcodec  # to be sure to be in a virtual env
    pip install --upgrade pip setuptools wheel
    pip -v install --editable .[doc,test]
    cutcutcodec test  # to test if the installation is ok
.. pip uninstall typing_extensions to debug attribute '__default__' of 'typing.ParamSpec' objects is not writable

You can now also compile documentation locally :ref:`documentation` and run the benchmark :ref:`testing` (after the previous step).


.. _documentation:

Build Documentation
-------------------

You can compile the documentation yourself after :ref:`full_installation`:

.. code:: shell

    cd docs/ && make clean && make html && cd -
    firefox docs/build/html/index.html


Platform Support
----------------

The tests were successful for theses configurations.

.. note::

    Contributors please test CutCutCodec on your platform then update this document and send a pull request.

+----------------------------------+------------------------+------------------------+-------------------------+
| Operating system                 | Tested Python versions | Tested FFmpeg versions | Tested architecture     |
+==================================+========================+========================+=========================+
| Linux Mint 21.3                  | 3.9, 3.10, 3.11        | 4.4.2                  | x86-64 2 cpu t6600      |
+----------------------------------+------------------------+------------------------+-------------------------+
| Ubuntu 24.10                     | 3.13                   | 7.0.2                  | x86-64 22 cpu core i7   |
+----------------------------------+------------------------+------------------------+-------------------------+