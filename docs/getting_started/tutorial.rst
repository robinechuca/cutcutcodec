.. _tutorial:

Tutorial
========

Before keeping reading, please make sure the installation is done :ref:`installation`.

The overall structure of the code will always look like this:

.. code-block:: python
    :linenos:

    from cutcutcodec.core.io import read, write  # for input / output
    from cutcutcodec.utils import get_project_root  # import usefull functions
    container = read(get_project_root().parent / "media" / "intro.webm")  # open the file
    stream = container.out_streams[0]  # manage one stream
    final_stream = stream  # apply filters
    write([final_stream], "/tmp/my_video.mp4", [{"encodec": "libx264", "rate": 24, "shape": (512, 512)}])


.. _basic_examples:

Basic Examples
--------------

This section presents minimalist examples of simple handling.

.. toctree::
    :numbered:
    :maxdepth: 1

    ../build/examples/basic/read.ipynb
    ../build/examples/basic/write.ipynb
    ../build/examples/basic/psnr_ssim.ipynb
    ../build/examples/basic/split_channels.ipynb


.. _advanced_examples:

Advanced Examples
-----------------

This section presents examples for more advanced topics.

.. toctree::
    :numbered:
    :maxdepth: 1

    ../build/examples/advanced/write_alpha.ipynb
    ../build/examples/advanced/multi_channels.ipynb
    ../build/examples/advanced/mandelbrot.ipynb
    ../build/examples/advanced/winer.ipynb
