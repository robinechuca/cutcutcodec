CutCutCodec |release|
=====================

.. include:: ../README.rst


Getting started
===============

.. toctree::
    :maxdepth: 2

    getting_started/installation.rst
    getting_started/tutorial.rst


API documentation
=================

.. toctree::
    :maxdepth: 2

    api_reference.rst


Developer guide
===============

.. toctree::
    :maxdepth: 2

    developer_guide/installation.rst
    developer_guide/testing.rst
    developer_guide/documentation.rst


Reference
=========

.. toctree::
    :maxdepth: 1

    modindex
    genindex
    changelog.rst

The documentation was generated on |today|.
