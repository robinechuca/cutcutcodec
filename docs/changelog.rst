.. _changelog:

What's new ?
============

For the complete list of changes, refer to the `git commits <https://framagit.org/robinechuca/cutcutcodec/-/network/main?ref_type=heads>`_.

1.0.1
-----

* Add a command line interface.
* Compiling dynamic expressions in C.

1.0.2
-----

* Add support for ``ffmpeg 6``.
* Able to compile ``atan`` function.
* Handling of non-square pixel readings.

1.0.3
-----

* Improved ergonomics of the "Entry Tabs" and "Export" window.
* Speed-up codec/encoder/muxer tests of compatibility by a factor 10.

1.0.4
-----

* Read images and SVG as well.

1.1.0
-----

* Add support for ``ffmpeg 7``.
* Refactor the documentation.
* Able to compile ``min`` and ``max`` functions.

1.1.1
-----

* Add a Winer audio denoise filter.

1.1.2
-----

* Add a video filter to change the speed.
* Add zero padding to audio frames.

1.1.3
-----

* Convert video colorspace to work in bt709.
* Support all read and write bit depth and channels.
* Able to compile ``==, >=, <=, >, >, if-else`` function.

1.1.4
-----

* Video frame are only in ``float32``.
* More image format supported.

1.2.1
-----

* Take care of color range.
* C implemenetation of the ``mse``, ``psnr`` and ``ssim`` metrics.
