#!/usr/bin/env python3

"""Allows assembly graphs to be represented in different forms.

Some representations are suitable for the generation,
others for data persistence and others for mathematical graph manipulation.
"""
