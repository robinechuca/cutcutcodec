/* Multithreading tools. */

#include <unistd.h>
#include <sys/syscall.h>
#ifndef SYS_gettid
#error 'SYS_gettid unavailable on this system'
#endif
#define gettid() ((pid_t)syscall(SYS_gettid))


long int set_num_threads(long int threads) {
  /* Set the number of thread for the openmp directives. */
  long int ncpu;
  if ( threads == 0 ) {
    if ( gettid() == getpid() ) {
    ncpu = omp_get_num_procs();
      omp_set_num_threads(ncpu);
    } else {
      omp_set_num_threads(1);
      ncpu = 1;
    }
  } else if ( threads < 0 ) {
    ncpu = omp_get_num_procs();
    omp_set_num_threads(ncpu);
  } else {
    omp_set_num_threads(threads);
    ncpu = threads;
  }
  return ncpu;
}
